angular.module('ang').config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {

   $locationProvider.hashPrefix('');
   $routeProvider
    // .when("/", {
    //     templateUrl: "home.html",
    //     controller: "HomeController"
    // })
    .when("/menu", {
        templateUrl: "menu.html",
        controller: "MenuController"
    })
    .otherwise({
        redirectTo: "/"
    })

}]);