angular.module('ang').controller('MenuController', ['$scope', function($scope) {

    $scope.comidas = [
        { id: 1, nombre: "Arroz", calorias: 30, visible: true },
        { id: 2, nombre: "Sopa", calorias: 90, visible: true },
        { id: 3, nombre: "Jugo", calorias: 300, visible: true },
        { id: 4, nombre: "Huevo", calorias: 200, visible: true }
    ];
    $scope.comidasSeleccionadas = []

    /**
     * Validar si la comida cumple con las calorias máximas
     * ingresadas por el usuario
     * @paramCalorias Calorias de la comida
     */
    $scope.validarCalorias = function(paramCalorias){
        return (paramCalorias <= $scope.caloriasMaximas) ? true : false;
    };

    $scope.actualizarComidas = function(){
        if($scope.caloriasMaximas == ''){
            angular.forEach($scope.comidas, function(value, key) {
                value.visible = true;
            });
        }else{
            if($scope.caloriasMaximas != null){
                angular.forEach($scope.comidas, function(value, key) {
                    value.visible = (value.calorias <= $scope.caloriasMaximas) ? true : false;
                });
            }
        }
    };

    /**
     * 
     */
    $scope.adicionarComida = function(paramIdComida){
        if($scope.comidasSeleccionadas.includes(paramIdComida)){
            // Si existe se elimina
            $scope.comidasSeleccionadas = $scope.comidasSeleccionadas.filter(value => {
                return value != paramIdComida;
            });
        }else{
            // si no existe se guarda
            $scope.comidasSeleccionadas.push(paramIdComida);
        }
    };

    $scope.actualizarComidas();
    
}]);